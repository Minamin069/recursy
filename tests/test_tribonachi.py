from tribonachi import tribonachi

def test_good():
    assert tribonachi(8) == 24, "Введено не число 8!"

def test_bad():
    assert tribonachi(8)!= 24, "Вы всё-таки ввели 8? Ужас"
