def tribonachi(n, a=0, b=0, c=1, n1 = 2):
    while n1 < n:
        sum = a+b+c
        a = b
        b = c
        c = sum
        n1 += 1
        tribonachi(n, a, b, c, n1)
    return c

if __name__ == "__main__":
    n = int(input("Введите номер числа, которое нужно вывести: "))
    print(tribonachi(n))
